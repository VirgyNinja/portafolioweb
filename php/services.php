<div id="service" class="section md-padding">

		<!-- Container -->
		<div class="container">

			<!-- Row -->
			<div class="row">

				<!-- Section header -->
				<div class="section-header text-center">
					<h2 class="title">Servicios</h2>
				</div>
				<!-- /Section header -->

				<!-- service -->
				<div class="col-md-6 col-sm-6">
					<div class="service">
						<i class="fa fa-laptop"></i>
						<h3>App Development</h3>
						<p>Desarrollo de aplicaciones web en tiempo real com PHP Codeigniter, jQuery, AJAX que se ajusten a lo que el cliente necesita.</p>
					</div>
				</div>
				<!-- /service -->

				<!-- service -->
				<div class="col-md-6 col-sm-6">
					<div class="service">
						<i class="fa fa-desktop"></i>
						<h3>Web Desing</h3>
						<p>Diseño de paginas web implementando HTML5, CSS y bootstap, aplicando el diseño responsivo.</p>
					</div>
				</div>
				<!-- /service -->

			</div>
			<!-- /Row -->

		</div>
		<!-- /Container -->
<br><br>
	</div>