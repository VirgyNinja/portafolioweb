<div id="team" class="section md-padding">

		<!-- Container -->
		<div class="container">

			<!-- Row -->
			<div class="row">

				<!-- Section header -->
				<div class="section-header text-center">
					<h2 class="title">Developer</h2>
				</div>
				<!-- /Section header -->

				<!-- team -->
				<div class="col-sm-4">
				</div>
				<!-- /team -->

				<!-- team -->
				<div class="col-sm-4">
					<div class="team">
						<div class="team-img">
							<img class="img-responsive" src="img/team2.png" alt="" style="max-width: 50%;margin-left: 77px;">
							<div class="overlay">
								<!-- <div class="team-social">
									<a href="#"><i class="fa fa-facebook"></i></a>
									<a href="#"><i class="fa fa-google-plus"></i></a>
									<a href="#"><i class="fa fa-twitter"></i></a>
								</div> -->
							</div>
						</div>
						<div class="team-content">
							<h3>Virgilio Antonio Ramos López</h3>
							<span>Web Designer and Developer</span>
						</div>
					</div>
				</div>
				<!-- /team -->

				<!-- team -->
				<div class="col-sm-4">
				</div>
				<!-- /team -->

			</div>
			<!-- /Row -->

		</div>
		<!-- /Container -->

	</div>