<header id="home">
		<!-- Background Image -->
		<div class="bg-img" style="background-image: url('img/background1.png');">
			<div class="overlay"></div>
		</div>
		<!-- /Background Image -->

		<!-- Nav -->
		<nav id="nav" class="navbar nav-transparent">
			<div class="container">

				<div class="navbar-header">
					<!-- Logo -->
					<!-- <div class="navbar-brand">
						<a href="index.html" style="color: white;font-size: 2em;">
							DEVELOPER
						</a>
					</div> -->
					<!-- /Logo -->

					<!-- Collapse nav button -->
					<div class="nav-collapse">
						<span></span>
					</div>
					<!-- /Collapse nav button -->
				</div>

				<!--  Main navigation  -->
				<ul class="main-nav nav navbar-nav navbar-right">
					<li><a href="#home">Inicio</a></li>
					<li><a href="#service">Servicios</a></li>
					<li><a href="#features">Portafolio</a></li>					
					<li><a href="#team">Team</a></li>
				</ul>
				<!-- /Main navigation -->

			</div>
		</nav>
		<!-- /Nav -->

		<!-- home wrapper -->
		<div class="home-wrapper" id="start">
			<div class="container">
				<div class="row">

					<!-- home content -->
					<div class="col-md-10 col-md-offset-1">
						<div class="home-content">
							<h1 class="white-text">WEB DESIGN AND DEVELOPER</h1>
							<p class="white-text">Diseño de sitios web responsivos y desarrollo de aplicaciones web PHP con Codeigniter.
							</p>
							<a class="white-btn" href="#service">Iniciemos!</a>
						</div>
					</div>
					<!-- /home content -->

				</div>
			</div>
		</div>
		<!-- /home wrapper -->

	</header>