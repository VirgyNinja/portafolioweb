<div id="features" class="section md-padding bg-grey">

		<!-- Container -->
		<div class="container">

			<!-- Row -->
			<div class="row">
				<!-- Section header -->
				<div class="section-header text-center">
					<h2 class="title">Proyectos realizados</h2>
				</div>
				<!-- /Section header -->
				<!-- why choose us content -->
				<div class="col-md-6">
					<div class="section-header">
						<h2 class="title">Visional web</h2>
					</div>
					<p>Sitio Web Informativo para promocionar servicios en aplicaciones para restaurantes administración interna y toma de pedidos a domicilio.</p>
					<div class="feature">
						<i class="fa fa-check"></i>
						<p>WordPress.</p>
					</div>
				</div>
				<!-- /why choose us content -->

				<!-- About slider -->
				<div class="col-md-6">
					<div id="about-slider" class="owl-carousel owl-theme">
						<img class="img-responsive" src="img/visio/home1.png" alt="">
						<img class="img-responsive" src="img/visio/home2.png" alt="">
						<img class="img-responsive" src="img/visio/contact.png" alt="">
						<img class="img-responsive" src="img/visio/test1.png" alt="">
						<img class="img-responsive" src="img/visio/test2.png" alt="">
					</div>
				</div>
				<!-- /About slider -->

			</div>
			<!-- /Row -->

			<!-- Row -->
			<div class="row">

				<!-- why choose us content -->
				<div class="col-md-6">
					
					<div id="about-slider2" class="owl-carousel owl-theme">
						<img class="img-responsive" src="img/cea/login.png" alt="">
						<img class="img-responsive" src="img/cea/alumno.png" alt="">
						<img class="img-responsive" src="img/cea/curri.png" alt="">
						<img class="img-responsive" src="img/cea/home.png" alt="">
						<img class="img-responsive" src="img/cea/inscri.png" alt="">
						<img class="img-responsive" src="img/cea/pago.png" alt="">
					</div>
					
				</div>
				<!-- /why choose us content -->

				<!-- About slider -->
				<div class="col-md-6">
					<div class="section-header">
						<h2 class="title">CEA</h2>
					</div>
					<p>Herramienta de registro académico para la administración de alumnos, gestión de usuarios, inscripción de cursos de vuelos distribuidos en tiempos, control de pagos, gestión de clases realizadas y faltantes, reporte de horas voladas por alumno, curricula por alumno y reporte de expediente del alumno.</p>
					<div class="feature">
						<i class="fa fa-check"></i>
						<p>PHP CodeIgniter.</p>
					</div>
					<div class="feature">
						<i class="fa fa-check"></i>
						<p>HTML5, CSS3.</p>
					</div>
					<div class="feature">
						<i class="fa fa-check"></i>
						<p>JavaScript, jQuery, Ajax.</p>
					</div>
					<div class="feature">
						<i class="fa fa-check"></i>
						<p>Bootstrap, Pages (template)..</p>
					</div>
				</div>
				<!-- /About slider -->

			</div>
			<!-- /Row -->

			<!-- Row -->
			<div class="row">

				<!-- why choose us content -->
				<div class="col-md-6">
					<div class="section-header">
						<h2 class="title">UORDER</h2>
					</div>
					<p>Herramienta para la gestión y toma de ordenes interna de pupuserias, administración de usuarios, categorías de comida personalizables, toma de órdenes, reportes de ventas del día, reportes de ventas semanales y mensuales y reportes de ventas por categoría de comida.</p>
					<div class="feature">
						<i class="fa fa-check"></i>
						<p> PHP CodeIgniter.</p>
					</div>
					<div class="feature">
						<i class="fa fa-check"></i>
						<p>JavaScript, jQuery, AJAX.</p>
					</div>
					<div class="feature">
						<i class="fa fa-check"></i>
						<p>HTML5, CSS3.</p>
					</div>
					<div class="feature">
						<i class="fa fa-check"></i>
						<p>Materialize.</p>
					</div>
				</div>
				<!-- /why choose us content -->

				<!-- About slider -->
				<div class="col-md-6">
					<div id="about-slider" class="owl-carousel owl-theme">
						<img class="img-responsive" src="img/uorder/login.png" alt="">
						<img class="img-responsive" src="img/uorder/home.png" alt="">
						<img class="img-responsive" src="img/uorder/cate.png" alt="">
						<img class="img-responsive" src="img/uorder/mesa.png" alt="">
						<img class="img-responsive" src="img/uorder/pendi.png" alt="">
						<img class="img-responsive" src="img/uorder/repor.png" alt="">
					</div>
				</div>
				<!-- /About slider -->

			</div>
			<!-- /Row -->

			<!-- Row -->
			<div class="row">

				<!-- why choose us content -->
				<div class="col-md-6">
					<div class="section-header">
						<h2 class="title">SISRA</h2>
					</div>
					<p>Administrar Expediente Alumnos, registro de notas por año y periodos, generar reportes (Listados de asistencia de alumnos, reporte de notas por periodo y materias, boleta de notas, generar el listado de paquetes escolares por rubro) etc.</p>
					<div class="feature">
						<i class="fa fa-check"></i>
						<p>PHP, CodeIgniter.</p>
					</div>
					<div class="feature">
						<i class="fa fa-check"></i>
						<p>JavaScript, jQuery, AJAX.</p>
					</div>
					<div class="feature">
						<i class="fa fa-check"></i>
						<p>HTML5, CSS3.</p>
					</div>
					<div class="feature">
						<i class="fa fa-check"></i>
						<p>Materialize.</p>
					</div>
				</div>
				<!-- /why choose us content -->

				<!-- About slider -->
				<div class="col-md-6">
					<div id="about-slider" class="owl-carousel owl-theme">
						<img class="img-responsive" src="img/sisra/login.png" alt="">
						<img class="img-responsive" src="img/sisra/conf.png" alt="">
						<img class="img-responsive" src="img/sisra/dato.png" alt="">
						<img class="img-responsive" src="img/sisra/gest.png" alt="">
						<img class="img-responsive" src="img/sisra/home1.png" alt="">
					</div>
				</div>
				<!-- /About slider -->

			</div>
			<!-- /Row -->

		</div>
		<!-- /Container -->

	</div>